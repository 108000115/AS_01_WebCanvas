# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | N         |
| Download                                         | 5%        | N         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
	Brush 可以當成筆筆，然後旁邊的slider可以調整大小
    Shape有形狀能選，但還是有bug
	Text 可以印出輸入的字
    redo undo 如描述

### Function description



### Gitlab page link

    your web page URL, which should be "https://108000115.gitlab.io/AS_01_WebCanvas"

### Others (Optional)

    Anythinh you want to say to TAs.

